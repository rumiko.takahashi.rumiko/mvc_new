<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/7/18
 * Time: 8:05 PM
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

function debug($str){
    echo '<pre>';

    var_dump($str);

    echo '</pre>';
    exit;
}