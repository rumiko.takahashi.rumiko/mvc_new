<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/7/18
 * Time: 8:26 PM
 */

namespace application\lib;

use PDO;


class Db
{

    protected $db;

    public function __construct()
    {
        $config = require 'application/config/db.php';

        $dsn = "mysql:host={$config['host']};dbname={$config['name']}";
        $this->db = new PDO($dsn, $config['user'], $config['password']);
    }

//    public function query($sql){
//         $query = $this->db->query($sql);
//         $result = $query->fetchColumn();
//    }
}