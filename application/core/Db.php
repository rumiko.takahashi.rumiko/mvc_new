<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/12/18
 * Time: 7:53 PM
 */

namespace application\core;

use PDO;
use PDOException;

class Db
{
    public static function getConnect(){
        $paramPath = 'application/config/db.php';
        $params = include ($paramPath);


        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        try {
            $db = new PDO($dsn, $params['user'], $params['password']);
        } catch (PDOException $db){
            die($db->getMessage());
        }

        $db->exec("set names utf-8");

        return $db;
    }
}