<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/10/18
 * Time: 3:40 PM
 */

namespace application\controller;

use application\core\Controller;


class AccountController extends Controller
{

    public function loginAction()
    {
        $this->view->render('Login');
    }

    public function registerAction()
    {

        if(isset($_POST['submit'])){
            $user['login'] = $_POST['login'];
            $user['password'] = $_POST['password'];

        }
        $this->view->render('Register');
    }

    public function deleteAction(){
        $this->view->render('Delete');
    }
}