<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/9/18
 * Time: 9:01 PM
 */

namespace application\controller;

use application\core\Controller;

use application\core\Db;
use application\models\Main;


class MainController extends Controller
{
    public function indexAction()
    {
        $db = Db::getConnect();

        $newsItem = Main::getAllNes();


        $this->view->render('Main page', $newsItem);
    }

}
